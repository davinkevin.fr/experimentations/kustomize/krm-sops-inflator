#!/bin/bash
set -euo pipefail

mkdir -p $XDG_CONFIG_HOME/sops/age/
echo $AGE_KEY > $XDG_CONFIG_HOME/sops/age/keys.txt

WORKING_DIR=$(mktemp -d)
cd $WORKING_DIR

KFN_INPUT="$WORKING_DIR/input.yaml"
cat > $KFN_INPUT

SOPS_SECRET="$WORKING_DIR/secret.yaml"
cat $KFN_INPUT | \
    yq e '.functionConfig' - | \
    yq 'del(.metadata.annotations."config.kubernetes.io/function")' - | \
    yq 'del(.metadata.annotations."config.kubernetes.io/local-config")' - | \
    yq '.kind = "Secret"' - | \
    yq '.apiVersion = "v1"' - \
    > $SOPS_SECRET

sops -d -i --ignore-mac $SOPS_SECRET

RESULT="$WORKING_DIR/output.yaml"
echo "kind: ResourceList" >> $RESULT
echo "items:" >> $RESULT
cat $SOPS_SECRET | yq ea '[.]' - >> $RESULT

cat $RESULT
